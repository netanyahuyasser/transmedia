<?php 

session_start();
if(!$_SESSION['logged']){
  header("Location: formlogin.php");
  exit;
}

// Setelah berhasil login
require_once 'php/config.php';
require 'include/header.php';
require 'include/sidebar.php';

?>

  <!-- /. NAV SIDE  -->
        <div id="page-wrapper">
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="page-head-line">TRANSMEDIA</h1>
                        <h1 class="page-subhead-line">MILIK KITA BERSAMA </h1>

                    </div>
                </div>
                <!-- /. ROW  -->
                <div class="row">
                    <div id="folderlist" class="col-md-4">
                        <div class="main-box mb-red">
                            <a href="view.php">
                                <i class="fa fa-folder fa-5x"></i>
                                <h5>FOLDER 1</h5>
                            </a>
                        </div>
                 
                        </div>
                        
                    </div>
                    
    <script type="text/javascript" src="assets/js/jquery-3.1.0.min.js"> </script>

