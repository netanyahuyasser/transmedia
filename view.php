<?php
session_start(); 
include_once 'php/dbconfig.php';
require 'include/header.php';
require 'include/sidebar.php';
?>

 <div id="page-wrapper">
            <div id="page-inner">

<a href="indexx.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon"></i>Uploads</a>


<div class="row">
    <table class='table table-bordered table-responsive'>
   
    <tr>
     <th>ID</th>
     <th>File</th>
     <th>Size</th>
     <th>Type</th>
     <th>Uploader</th>
     <th colspan="2" align="center">Actions</th>
    </tr>
  
	
    <?php
        $query = "SELECT * FROM upload";       
        $records_per_page=10;
        $newquery = $crud->paging($query,$records_per_page);
        $crud->dataview($newquery);
     ?>
      <tr>
        <td colspan="9" align="center">
            <div class="pagination-wrap">
                <?php $crud->paginglink($query,$records_per_page); ?>
            </div>
        </td>
    </tr>
    </table>
</div>
</div>    
</div>
</body>
</html>