<?php
session_start();

include_once 'dbconfig.php';

if(isset($_POST['btn-del']))
{
    $id = $_GET['delete_id'];
    $crud->delete($id);
    header("Location: delete.php?deleted"); 
}

?>



<div class="clearfix"></div>

<div class="container">

    <?php
    if(isset($_GET['deleted']))
    {
        ?>
        <div class="alert alert-success">
        <strong>Success!</strong> record was deleted... 
        </div>
        <?php
    }
    else
    {
        ?>
        <div class="alert alert-danger">
        <strong>Sure !</strong> to remove the following record ? 
        </div>
        <?php
    }
    ?>  
</div>

<div class="clearfix"></div>

<div class="container">
    
     <?php
     if(isset($_GET['delete_id']))
     {
         ?>
         <table class='table table-bordered'>
         <tr>
         <th>#</th>
         <th>File</th>
         <th>Size</th>
         <th>Tipe File</th>
        
         </tr>
         <?php
         $stmt = $DB_con->prepare("SELECT * FROM upload WHERE id=:id");
         $stmt->execute(array(":id"=>$_GET['delete_id']));
         while($row=$stmt->fetch(PDO::FETCH_BOTH))
         {
             ?>
             <tr>
             <td><?php print($row['id']); ?></td>
             <td><?php print($row['file']); ?></td>
             <td><?php print($row['size']); ?></td>
             <td><?php print($row['type']); ?></td>
            
             </tr>
             <?php
         }
         ?>
         </table>
         <?php
     }
     ?>
</div>

<div class="container">
<p>
<?php
if(isset($_GET['delete_id']))
{
    ?>
    <form method="post">
    <input type="hidden" name="id" value="<?php echo $row['id']; ?>" />
    <button class="btn btn-large btn-primary" type="submit" name="btn-del"><i class="glyphicon glyphicon-trash"></i> &nbsp; YES</button>
    <a href="../view.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; NO</a>
    </form>  
    <?php
}
else
{
    ?>
    <a href="../view.php" class="btn btn-large btn-success"><i class="glyphicon glyphicon-backward"></i> &nbsp; Back to index</a>

    <?php
}
?>
</p>
</div>  
<!--?php include_once 'footer.php'; ?-->