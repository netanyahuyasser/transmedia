﻿<?php
  define('dbServer', 'localhost');
  define('dbDatabase', 'transmedia');
  define('dbUser', 'root');
  define('dbPass', '');
  /**
   * Connect to MSSQL Server and instantiate the PDO object.
   */
  try {
    $pdo = new PDO(
        "mysql:host=" . dbServer . ";dbname=" . dbDatabase, //DSN
        dbUser, //Username
        dbPass //Password
    );
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch(PDOException $e) {
    die('Could not connect to the database:<br/>' . $e);
  }
?>
